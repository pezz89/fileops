import audiofile

def downmix(AudioFile, output_channels=1):
    """
    Takes an audio file and downmixes audio to the number of channels specified
    Can currently downmix from:
        5.1
        2 (Stereo)
    To:
        2 (Stereo)
        1 (Mono)
    """

    return AudioFile

